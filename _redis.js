// const redis = require('redis');

// const redisClient = redis.createClient(6379, '0.0.0.0')
// const util = require('util');

// redisClient.on("error", (error) => {
//     console.log(error);
// });

const util = require("util");
const redis = require("redis");
const { response } = require("express");
const redisUrl = "redis://127.0.0.1:6379";
const client = redis.createClient(redisUrl);
client.get = util.promisify(client.get);

exports.getRedis = async (Keys) => {
  let isDataAlreadyCached = await client.get(Keys);
  if (!isDataAlreadyCached) {
      return false;
    }
  return isDataAlreadyCached;
};

exports.setRedis = async(Keys, data) => {
    client.set(Keys, data);
    client.expire(Keys, 3600)
};
