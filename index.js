require("dotenv").config();
let express = require("express");
let response = require("./_libs/response");
let bodyParser = require("body-parser");
let mongoose = require("mongoose");
let cors = require("cors");
const redis = require('redis');

// const redisClient = redis.createClient(6379, '0.0.0.0')

// redisClient.on('error', (err) => {
    // console.log('Error occured while connecting or accessing redis server');
// });

// if(!redisClient.get('customer_name', redis.print)){
//     //create a new record
//     redisClient.set('customer_name','John Doe', redis.print);
//     console.log('Writing Property : customer_name');
// } else {
//     let val = redisClient.get('customer_name',redis.print);
//     console.log(`Reading property : customer_name - ${val}`);
// }
 
//routes 
let usersRoute = require('./route.users');
let authRoute = require('./route.auth');



let app = express();
app.use(cors())
app.use(
    bodyParser.urlencoded({
        extended:true
    })
)
app.use(bodyParser.json());

//mongoose
mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser:true,
    // userUnifiedTopology:true,
    // useCreatedIndex:tr
});

var db = mongoose.connection;


// Added check for DB connection
if (!db) console.log("Error Connecting to DB!");
else console.log("DB Connected!");
// end mongoose

var port = process.env.SERVICE_PORT || 4000;

app.get("/", (req, res) =>  response.success(res, "Welcome to " + process.env.SERVICE_NAME + "-service"));


//vi Routes
app.use("/api/v1", authRoute);
app.use("/api/v1", usersRoute);

app.use((req,res) => response.notFound(res,"Endpoint not found!"));

app.listen(port, function(){
    console.log("Running " + process.env.SERVICE_NAME + "-service on port" + port);
})



