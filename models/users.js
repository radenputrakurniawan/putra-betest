"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var UsersSchemas = new Schema({
  userName: { type: String, required: "Username is required", unique: true,},
  accountNumber: { type: Number, default: 0, unique: true, },
  emailAddress: { type: String, required:"Email is Required", unique: true,},
  identityNumber: { type: Number, default: 0, unique: true,},
});

UsersSchemas.index({userName: 1, emailAddress: 1},{unique: true});

module.exports = mongoose.model("Users", UsersSchemas);
