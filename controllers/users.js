"use strict";

const Users = require("../models/users");
const response = require("../_libs/response");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { getRedis, setRedis } = require("../_redis");

exports.getAllUsers = async function (req, res) {
  let keys = "data:" + req.decoded.data.userName;
  let cachedData = await getRedis(keys);
  if (cachedData) {
    response.success(res, "Get all user data success!", JSON.parse(cachedData));
  } else {
    Users.find()
      .then((data) => {
        setRedis(keys, JSON.stringify(data));
        response.success(res, "Get all user data success!", data);
      })
      .catch((err) => response.badRequest(res, err.message));
  }
};

exports.createUser = function (req, res) {
  // req.body['identityNumber'] = bcrypt.hashSync(req.body['identityNumber'], parseInt(process.env.BCRYPT_ROUNDS));
  Users.create(req.body)
    .then((data) => {
      response.created(res, "User data created!", data);
    })
    .catch((err) => response.badRequest(res, err.message));
};

exports.getUserByAccountNumberAndIdentityNumber = function (req, res) {
  Users.findOne({
    $or: [
      { accountNumber: req.params.search },
      { IdentityNumber: req.params.search },
    ],
  })
    .then((data) => {
      if (!data) {
        response.notFound(res, "User not found!");
      }
      response.success(res, "Get user by id success!", data);
    })
    .catch((err) => response.badRequest(res, err.message));
};

exports.updateUser = function (req, res) {
  Users.findOneAndUpdate({ _id: req.params.search }, req.body)
    .then((data) => {
      if (!data) {
        response.notFound(res, "User not found!");
      }
      response.success(res, "Update user data success!");
    })
    .catch((err) => response.badRequest(res, err.message));
};

exports.deleteUser = function (req, res) {
  Users.findOneAndDelete({ _id: req.params.search })
    .then((data) => {
      if (!data) {
        response.notFound(res, "User not found!");
      }
      response.success(res, "Delete user data success!");
    })
    .catch((err) => response.badRequest(res, err.message));
};

exports.findByCredentials = function (req, res) {
  // Find if user exists
  if (!req.body.username && !req.body.email) {
    response.badRequest(res, "Username or email is required!");
  }

  Users.findOne({
    $or: [{ userName: req.body.username }, { emailAddress: req.body.email }],
  })
    .then((result) => {
      if (!result) {
        response.badRequest(res, "Wrong user or identityNumber!");
      }
      console.log("data : ", result);
      return result;
    })
    .then((user) => {
      if (!req.body.identityNumber) {
        response.badRequest(res, "Wrong user or identityNumber!");
      }
      console.log(user);
      let token = jwt.sign(
        {
          data: user,
        },
        process.env.JWT_KEY,
        {
          expiresIn: process.env.JWT_EXPIRED,
        }
      );

      let responseData = {
        token: token,
        user: user
      };
      response.success(res, "User logged in!", responseData);
      return true;
    })
    .catch((err) => {
      response.badRequest(res, "Something went wrong!");
    });
};

exports.logout = function (req, res) {
  response.success(res, "Logout success!");
};
