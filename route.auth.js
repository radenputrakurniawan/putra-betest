"user strict";

let router = require('express').Router();
var usersController = require("./controllers/users");

//Auth Routes
router.route("/auth/login").post(usersController.findByCredentials);

router.route("/auth/logout").post(usersController.logout);

module.exports = router