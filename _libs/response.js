let responseFormat = {
    status: false,
    message:"",
    data:""
};

exports.success = (res, message, data) => {
    responseFormat["status"] = true;
    responseFormat["message"] = message;
    responseFormat["data"] = data;
    res.status(200).json(responseFormat);
}

exports.created = (res, message, data) => {
    responseFormat["status"] = true;
    responseFormat["message"] = message;
    responseFormat["data"] = data;
    res.status(201).json(responseFormat);
  }
  
  exports.notFound = (res, message, data) => {
    responseFormat["status"] = false;
    responseFormat["message"] = message;
    responseFormat["data"] = data;
    res.status(404).json(responseFormat);
  };
  
  exports.badRequest = (res, message, data) => {
    responseFormat["status"] = false;
    responseFormat["message"] = message;
    responseFormat["data"] = data;
    res.status(400).json(responseFormat);
  };
  
  exports.unauthorized = (res, message, data) => {
    responseFormat["status"] = false;
    responseFormat["message"] = message;
    responseFormat["data"] = data;
    res.status(401).json(responseFormat);
  }
  
  exports.forbidden = (res, message, data) => {
    responseFormat["status"] = false;
    responseFormat["message"] = message;
    responseFormat["data"] = data;
    res.status(403).json(responseFormat);
  }
  
  exports.methodNotAllowed = (res, message, data) => {
    responseFormat["status"] = false;
    responseFormat["message"] = message;
    responseFormat["data"] = data;
    res.status(405).json(responseFormat);
  }
  
  exports.internalServerError = (res, message, data) => {
    responseFormat["status"] = false;
    responseFormat["message"] = message;
    responseFormat["data"] = data;
    res.status(500).json(responseFormat);
  }
  