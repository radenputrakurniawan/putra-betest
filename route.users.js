"use strict";

let router = require('express').Router();
let middleware = require('./_middleware');
var usersController = require("./controllers/users");

router
.route("/register")
.post(usersController.createUser);


router
    .use(middleware.checkToken) 
    .route("/users")
    .get(usersController.getAllUsers)



router
    .use(middleware.checkToken)
    .route("/user/:search")
    .get(usersController.getUserByAccountNumberAndIdentityNumber)
    .put(usersController.updateUser)
    .delete(usersController.deleteUser);
  

    module.exports = router;


    